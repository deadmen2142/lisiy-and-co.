#include "stdafx.h"

enum FigureType { RECTANGLE, CIRCLE };
enum Color { RED, ORANGE, YELLOW, GREEN, BLUE, DARKBLUE, PURPLE };

struct Point
{
	int x;
	int y;

	Point()
	{
		x = 0;
		y = 0;
	}

	Point(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
};

struct FlatFigure
{
public:
	FigureType figureType;
	Color color;
	void* figureData;
};

struct Rectangle
{
public:
	Point leftUp;
	Point rigthDown;

	Rectangle(Point _leftUp, Point _rightDown)
	{
		leftUp = _leftUp;
		rigthDown = _rightDown;
	}
};

struct Circle
{
public:
	Point center;
	int radius;

	Circle(Point _center, int _radius)
	{
		center = _center;
		radius = _radius;
	}
};

struct Node
{
public:
	FlatFigure data;
	Node* next;
};

struct Queue
{
public:
	int length;
	Node* header;

	Queue()
	{
		length = 0;
		header = NULL;
	}
};

FlatFigure CreateFigure(Color color, Rectangle* figure)
{
	FlatFigure flatFigure;

	flatFigure.color = color;
	flatFigure.figureType = RECTANGLE;
	flatFigure.figureData = figure;

	return flatFigure;
}

FlatFigure CreateFigure(Color color, Circle* figure)
{
	FlatFigure flatFigure;

	flatFigure.color = color;
	flatFigure.figureType = CIRCLE;
	flatFigure.figureData = figure;

	return flatFigure;
}

void Push(Queue* queue, FlatFigure figure)
{
	Node* newNode = new Node();

	newNode->data = figure;
	newNode->next = NULL;

	if (queue->length > 0)
	{
		Node* iterator = queue->header;

		for (int i = 1; i < queue->length; i++)
		{
			iterator = iterator->next;
		}

		iterator->next = newNode;
		queue->length = queue->length + 1;

		return;
	}
	else if (queue->length == 0)
	{
		queue->header = newNode;
		queue->length = 1;

		return;
	}
	else
	{
		printf("Queue error: incorrect queue length.");
	}
}

FlatFigure Pop(Queue* queue)
{
	FlatFigure flatFigure = queue->header->data;

	if (queue->length > 1)
	{
		Node* newHeader = queue->header->next;

		delete[] queue->header;

		queue->header = newHeader;
		queue->length = queue->length - 1;

		return flatFigure;
	}
	else
	{
		delete[] queue->header;

		queue->header = NULL;
		queue->length = 0;

		return flatFigure;
	}
}

void ClearQueue(Queue *queue)
{
	int length = queue->length;

	for (int i = 0; i < length; i++)
	{
		Pop(queue);
	}
}

void DeleteQueue(Queue *queue)
{
	ClearQueue(queue);

	delete[] queue;
}

int main()
{
	Queue *queue = new Queue();

	Rectangle rectangle(Point(1, 1), Point(2, 4));
	Circle circle(Point(1, 4), 3);
	Rectangle rectangle2(Point(4, 1), Point(2, 8));

	FlatFigure figure1 = CreateFigure(BLUE, &rectangle);
	FlatFigure figure2 = CreateFigure(ORANGE, &circle);
	FlatFigure figure3 = CreateFigure(RED, &rectangle2);

	Push(queue, figure1);
	Push(queue, figure2);
	Push(queue, figure3);
	Push(queue, figure1);

	Pop(queue);

	ClearQueue(queue);

	Push(queue, figure3);

	DeleteQueue(queue);

	return 0;
}




